# README #

You need Xcode to build this project.

This app generates tilings of the plane using finite sets of tiles.

Usage
--------
* Rotate your iOS device through physical space and watch as the tilings morph on the screen.
* Drag your finger on the screen to scroll the tiling.
* Press and hold to bring up a menu of options.

The Mathematics
---------------
What follows is some mathsy explanation if you are interested.

This app shows a visualisation of slicing an n-dimensional lattice of points with a 2-dimensional plane. As you rotate your iOS device, you are rotating this plane through n-space. Certain points from the lattice are projected onto the plane, forming the vertices in the tiling. The lattice points that are selected are those lying inside any n-dimensional hypercube of unit edge length, oriented parallel to the lattice, whose centre lies on the plane.

### Who do I talk to? ###

* Ruben Zilibowitz