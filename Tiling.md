Tiling
======
This app generates tilings of the plane using finite sets of tiles.

Usage
--------
* Rotate your iOS device through physical space and watch as the tilings morph on the screen.
* Drag your finger on the screen to scroll the tiling.
* Press and hold to bring up a menu of options.

The Mathematics
---------------
What follows is a somewhat technical description of the maths involved in this app. It is not necessary to understand this in order to use the app.

You are slicing an n-dimensional lattice of points with a 2-dimensional plane. As you rotate your iOS device, you are rotating this plane through n-space. Certain points form the lattice are projected onto the plane, forming the verticies in the tiling. The lattice points that are selected are those lying inside any n-dimensional hypercube of unit edge length, oriented parallel to the lattice, whose centre lies on the plane.

-----

Version {VERSION_STRING}  
Copyright © 2015, Ruben Zilibowitz.  
All rights reserved.
