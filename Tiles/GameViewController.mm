//
//  GameViewController.m
//  Tiles
//
//  Created by Ruben Zilibowitz on 2/01/2015.
//  Copyright (c) 2015 Zilibowitz Productions. All rights reserved.
//

// See this URL for more info
// http://www.geom.uiuc.edu/apps/quasitiler/about.html#numbers

#import "GameViewController.h"
#import "UIApplication+Version.h"
#import "EmitterTemplate.h"
#import "EmitterShader.h"
#import "ColoursSchemeMenu.h"
#import "HelpViewController.h"
#import <CoreMotion/CoreMotion.h>

#include "Lattice_Zn.h"
#include "RZMatrix.h"
#include "RZQuaternion.h"

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

CGContextRef CreateARGBBitmapContext (CGImageRef inImage);

extern NSString * const RZRandomiseOrientation;
extern NSString * const RZColourNotification;
extern NSString * const RZDimensionNotification;
extern NSString * const RZOpenHelpNotification;

extern const std::vector< const std::vector<unsigned long>* > ColourSchemes;

@interface GameViewController ()

// Properties
@property (strong) EmitterShader* emitterShader;
@property (nonatomic,strong) CMMotionManager *motionManager;
@property (nonatomic,strong) UIPopoverController *currentPopoverController;
@property (nonatomic,strong) UIImage *currentImage;

@end

// Dimensionality should be at least 3.
// Probably not more than 10 or 12.
// Higher values will be slower.
//const int _dimensionality = 6;

template <typename T>
std::array<T,3> CrossProduct1D(std::array<T,3> const &a, std::array<T,3> const &b)
{
    std::array<T,3> r;
    r[0] = a[1]*b[2]-a[2]*b[1];
    r[1] = a[2]*b[0]-a[0]*b[2];
    r[2] = a[0]*b[1]-a[1]*b[0];
    return r;
}

template <int dim>
class Tiler {
    RZMatrix<dim, dim, double> _changeBasisMatrix;
    std::array<double, dim> _pivotPoint;
    CMRotationMatrix _cm;
    const std::vector<unsigned long>* _colourScheme;
    float   _scale;
    float   _aspectRatio;
    dispatch_queue_t _dataQueue1, _dataQueue2;
    
    typedef typename Lattice_Zn<dim>::NVector NVector;
    
public:
    void pinch(float velocity) {
        _scale += velocity * 0.001f;
    }
    
    void applyOrientation(const RZQuaternion<double> &oldQuat) {
        
        RZQuaternion<double> a = oldQuat.RotatePoint(RZQuaternion<double>::Point3D(1,0,0));
        RZQuaternion<double> b = oldQuat.RotatePoint(RZQuaternion<double>::Point3D(0,1,0));
        RZQuaternion<double> c = oldQuat.RotatePoint(RZQuaternion<double>::Point3D(0,0,1));
        
        dispatch_barrier_async(_dataQueue1, ^{
            /*
             _cm.m11 = a.x; _cm.m21 = b.x; _cm.m31 = c.x;
             _cm.m12 = a.y; _cm.m22 = b.y; _cm.m32 = c.y;
             _cm.m13 = a.z; _cm.m23 = b.z; _cm.m33 = c.z;
             */
            
            _cm.m11 = a.u.x; _cm.m21 = b.u.x; _cm.m31 = c.u.x;
            _cm.m12 = a.u.y; _cm.m22 = b.u.y; _cm.m32 = c.u.y;
            _cm.m13 = a.u.z; _cm.m23 = b.u.z; _cm.m33 = c.u.z;
            
            // or could just use the raw rotation matrix data
            // _cm = self.motionManager.deviceMotion.attitude.rotationMatrix;
        });
    }
    
    float randomFloatBetween(float min, float max)
    {
        float range = max - min;
        return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * range) + min;
    }
    
    void randomiseOrientation() {
        std::array<double, dim*(dim-1)/2> xs;
        for (int i = 0; i < xs.size(); i++) {
            xs[i] = randomFloatBetween(-2,2);
        }
        _changeBasisMatrix = CayleyTransform<dim,double>(xs);
    }
    
    void touchesMoved(CGPoint point, CGPoint lastPoint) {
        CGPoint dp = CGPointMake(point.x - lastPoint.x, point.y - lastPoint.y);
        
        __block std::array<double,3> A, B;
        dispatch_sync(_dataQueue1, ^{
            std::array<double,3> At = {_cm.m11,_cm.m12,_cm.m13};
            std::array<double,3> Bt = {_cm.m21,_cm.m22,_cm.m23};
            A = At;
            B = Bt;
        });
        auto C = CrossProduct1D(A, B);
        
        RZMatrix<dim, dim, double> mat = IdentityMatrix<dim, double>();
        mat(0,0) = A[0]; mat(0,1) = B[0]; mat(0,2) = C[0];
        mat(1,0) = A[1]; mat(1,1) = B[1]; mat(1,2) = C[1];
        mat(2,0) = A[2]; mat(2,1) = B[2]; mat(2,2) = C[2];
        
        std::array<double, dim> centerOffset;
        centerOffset.fill(0);
        centerOffset[0] = -dp.x * 0.02f;
        centerOffset[1] = dp.y * 0.02f;
        auto matCenterOffset = mat * centerOffset;
        
        dispatch_barrier_async(_dataQueue2, ^{
            for (int i = 0; i < dim; i++) {
                _pivotPoint[i] += matCenterOffset[i];
            }
        });
    }
    
    void doPoints() {
        
        NVector gen1;
        NVector gen2;
        NVector offset;
        __block std::array<double, 3> A, B;
        gen1.fill(0);
        gen2.fill(0);
        offset.fill(0);
        A.fill(0);
        B.fill(0);
        
        dispatch_sync(_dataQueue1, ^{
            // We want to use the iPad's rotation matrix to setup our plane
            if (std::abs(_cm.m11*_cm.m11 + _cm.m12*_cm.m12 + _cm.m13*_cm.m13 - 1) > 0.01f || std::abs(_cm.m21*_cm.m21 + _cm.m22*_cm.m22 + _cm.m23*_cm.m23 - 1) > 0.01f) {
                std::cerr << "Warning: rotation matrix is not special orthogonal" << std::endl;
                A[0] = 1;
                B[1] = 1;
            }
            else {
                A[0] = _cm.m11; A[1] = _cm.m12; A[2] = _cm.m13;
                B[0] = _cm.m21; B[1] = _cm.m22; B[2] = _cm.m23;
            }
        });
        
        auto C = CrossProduct1D(A, B);
        
        RZMatrix<dim, dim, double> mat = IdentityMatrix<dim, double>();
        mat(0,0) = A[0]; mat(0,1) = B[0]; mat(0,2) = C[0];
        mat(1,0) = A[1]; mat(1,1) = B[1]; mat(1,2) = C[1];
        mat(2,0) = A[2]; mat(2,1) = B[2]; mat(2,2) = C[2];
        __block std::array<double, dim> P;
        dispatch_sync(_dataQueue2, ^{
            P = InverseMatrix(mat) * _pivotPoint;
        });
        const double u = P[0];
        const double v = P[1];
        const double c = P[2];
        
        // we now apply a random rotation through N-dimensional space
        // this makes sure we have something interesting to look at
        {
            mat = _changeBasisMatrix * mat;
            for (int r = 0; r < dim; r++) {
                gen1[r] = mat(r,0);
                gen2[r] = mat(r,1);
                offset[r] = mat(r,2) * c;
            }
        }
        
        // now we can run the tiling generator
        // and output the results to opengl
        Emitter *myEmitter = new Emitter;
        RZColourPalette cols;
        cols.paletteFromHexVector(*_colourScheme);
        Lattice_Zn<dim> lat(myEmitter, gen1, gen2, offset, &cols, _aspectRatio, -u, -v, _scale, 0.005f, NULL);
        memset(myEmitter->particles, 0, sizeof(Particles)*NUM_PARTICLES);
        lat.traverseLatticeFrom(u, v);
        dispatch_async(dispatch_get_main_queue(), ^{
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Particles)*NUM_PARTICLES, myEmitter->particles);
            delete myEmitter;
        });
    }
    
    void applyColorScheme(const std::vector<unsigned long>* inColourScheme) {
        _colourScheme = inColourScheme;
    }
    
    Tiler(dispatch_queue_t queue1, dispatch_queue_t queue2, float aspectRatio) : _aspectRatio(aspectRatio), _scale(0.2f), _dataQueue1(queue1), _dataQueue2(queue2) {
        // initialise pivot point
        for (int i = 0; i < dim; i++) {
            _pivotPoint[i] = i*10.f;
        }
        
        // colours
        _colourScheme = ColourSchemes[0];
    }
    ~Tiler() {}
};

@implementation GameViewController
{
    // Instance variables
    float   _aspectRatio;
    float   _timeCurrent;
    float   _timeMax;
    int     _timeDirection;
    GLuint  _particleBuffer;
    double _roll, _pitch, _yaw;
    std::array<double, 2> _generator;
    Emitter _emitter;
    NSOperationQueue *updateQueue;
    dispatch_queue_t dataQueue1, dataQueue2;
    bool _updatingRotation;
    CGContextRef _imageContext;
    
    Tiler<3> *tiler3;
    Tiler<4> *tiler4;
    Tiler<5> *tiler5;
    Tiler<6> *tiler6;
    Tiler<7> *tiler7;
    Tiler<8> *tiler8;
    Tiler<9> *tiler9;
    Tiler<10> *tiler10;
    Tiler<11> *tiler11;
    Tiler<12> *tiler12;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (CMMotionManager*)motionManager {
    if (_motionManager == nil) {
        _motionManager = [[CMMotionManager alloc] init];
    }
    return _motionManager;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Set up context
    EAGLContext* context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    [EAGLContext setCurrentContext:context];
    
    [self.motionManager startDeviceMotionUpdates];
    
    // Set up view
    GLKView* view = (GLKView*)self.view;
    view.context = context;
    
    // enable multi-sampling (anti-aliasing)
    view.drawableMultisample = GLKViewDrawableMultisample4X;
    
    // Initialize variables
    //    _locationOffset = CGPointZero;
    _timeCurrent = 0.0f;
    _timeMax = 3.0f;
    _timeDirection = 1;
    _aspectRatio = view.frame.size.width / view.frame.size.height;
    if (view.frame.size.width < view.frame.size.height) {
        _aspectRatio = 1 / _aspectRatio;
    }
    
    // create dispatch queue
    updateQueue = [[NSOperationQueue alloc] init];
    updateQueue.maxConcurrentOperationCount = 1;
    dataQueue1 = dispatch_queue_create("data1", DISPATCH_QUEUE_CONCURRENT);
    dataQueue2 = dispatch_queue_create("data2", DISPATCH_QUEUE_CONCURRENT);
    
    // Init tilers
    tiler3 = nullptr;
    tiler4 = nullptr;
    tiler5 = nullptr;
    tiler6 = new Tiler<6>(dataQueue1,dataQueue2,_aspectRatio);
    tiler7 = nullptr;
    tiler8 = nullptr;
    tiler9 = nullptr;
    tiler10 = nullptr;
    tiler11 = nullptr;
    tiler12 = nullptr;
    
    // Load Shader
    [self loadShader];
    
    // Load Texture
    //    [self loadTexture:@"texture_64.png"];
    
    // Load Particle System
    [self loadParticles];
    [self loadEmitter];
    
    // compute out a random change of basis matrix
    [self randomiseOrientation:nil];
    
    _updatingRotation = true;
    
    // notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(randomiseOrientation:) name:RZRandomiseOrientation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedColourScheme:) name:RZColourNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dimension:) name:RZDimensionNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showHelp) name:RZOpenHelpNotification object:nil];
    
    // gestures
    id pressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [self.view addGestureRecognizer:pressGesture];
    id pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
    [self.view addGestureRecognizer:pinchGesture];
    
    [self performSelector:@selector(showHelp) withObject:nil afterDelay:0.5];
}

- (void)dealloc {
    [self.motionManager stopDeviceMotionUpdates];
}

- (void)showHelp {
    [self performSegueWithIdentifier:@"showHelp" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showHelp"]) {
        HelpViewController *dest = segue.destinationViewController;
        
        NSURL *fileURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Help" ofType:@"html"]];
        NSString *content = [NSString stringWithContentsOfURL:fileURL encoding:NSUTF8StringEncoding error:nil];
        content = [content stringByReplacingOccurrencesOfString:@"{VERSION_STRING}" withString:[UIApplication appVersion]];
        UIWebView *webView = (id)
        [dest.view viewWithTag:1];
        [webView loadHTMLString:content baseURL:fileURL];
    }
}

#pragma mark - Load Shader

- (void)loadShader
{
    self.emitterShader = [[EmitterShader alloc] init];
    [self.emitterShader loadShader];
    glUseProgram(self.emitterShader.program);
}

/*
 #pragma mark - Load Texture
 
 - (void)loadTexture:(NSString *)fileName
 {
 NSDictionary* options = [NSDictionary dictionaryWithObjectsAndKeys:
 [NSNumber numberWithBool:YES],
 GLKTextureLoaderOriginBottomLeft,
 nil];
 
 NSError* error;
 NSString* path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
 GLKTextureInfo* texture = [GLKTextureLoader textureWithContentsOfFile:path options:options error:&error];
 if(texture == nil)
 {
 NSLog(@"Error loading file: %@", [error localizedDescription]);
 }
 
 glBindTexture(GL_TEXTURE_2D, texture.name);
 }
 */
#pragma mark - Load Particle System

- (float)randomFloatBetween:(float)min :(float)max
{
    float range = max - min;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * range) + min;
}

- (void)loadParticles
{
    for(int i=0; i<NUM_PARTICLES; i++)
    {
        // Assign a random shade offset to each particle, for each RGB channel
        _emitter.particles[i].shade[0] = [self randomFloatBetween:-0.25f :0.25f];
        _emitter.particles[i].shade[1] = [self randomFloatBetween:-0.25f :0.25f];
        _emitter.particles[i].shade[2] = [self randomFloatBetween:-0.25f :0.25f];
        
        _emitter.particles[i].location[0] = 0;
        _emitter.particles[i].location[1] = 0;
    }
    
    // Create Vertex Buffer Object (VBO)
    glGenBuffers(1, &_particleBuffer);                   // Generate particle buffer
    glBindBuffer(GL_ARRAY_BUFFER, _particleBuffer);      // Bind particle buffer
    glBufferData(                                        // Fill bound buffer with particles
                 GL_ARRAY_BUFFER,                        // Buffer type (vertices/particles)
                 sizeof(_emitter.particles),             // Buffer data size
                 _emitter.particles,                     // Buffer data pointer
                 GL_DYNAMIC_DRAW);                       // Data changes
}

- (void)loadEmitter
{
    _emitter.k = 4.0f;           // Constant k
    _emitter.color[0] = 0.76f;   // Color: R
    _emitter.color[1] = 0.32f;   // Color: G
    _emitter.color[2] = 0.84f;   // Color: B
}

#pragma mark - GLKViewDelegate

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    // Set the background color (black)
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    // Set the blending function (normal w/ premultiplied alpha)
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    // Create Projection Matrix
    GLKMatrix4 projectionMatrix = GLKMatrix4MakeScale(1.0f, _aspectRatio, 1.0f);
    
    // Uniforms
    glUniformMatrix4fv(self.emitterShader.uProjectionMatrix, 1, 0, projectionMatrix.m);
    glUniform1f(self.emitterShader.uK, _emitter.k);
    glUniform3f(self.emitterShader.uColor, _emitter.color[0], _emitter.color[1], _emitter.color[2]);
    glUniform1f(self.emitterShader.uTime, (_timeCurrent/_timeMax));
    //    glUniform1i(self.emitterShader.uTexture, 0);
    
    // Attributes
    glEnableVertexAttribArray(self.emitterShader.aShade);
    glVertexAttribPointer(self.emitterShader.aShade,                // Set pointer
                          3,                                        // Three components per particle
                          GL_FLOAT,                                 // Data is floating point type
                          GL_FALSE,                                 // No fixed point scaling
                          sizeof(Particles),                        // No gaps in data
                          (void*)(offsetof(Particles, shade)));     // Start from "shade" offset within bound buffer
    
    glEnableVertexAttribArray(self.emitterShader.aLocation);
    glVertexAttribPointer(self.emitterShader.aLocation,             // Set pointer
                          2,                                        // Three components per particle
                          GL_FLOAT,                                 // Data is floating point type
                          GL_FALSE,                                 // No fixed point scaling
                          sizeof(Particles),                        // No gaps in data
                          (void*)(offsetof(Particles, location)));  // Start from "location" offset within bound buffer
    
    // Draw
    glDrawArrays(GL_TRIANGLES, 0, NUM_PARTICLES);
    glDisableVertexAttribArray(self.emitterShader.aShade);
    glDisableVertexAttribArray(self.emitterShader.aLocation);
}

double radiansToDegrees(double x) {
    return (180/M_PI)*x;
}

static unsigned long frameCount = 0;

- (void)update
{
    if(_timeCurrent > _timeMax)
        _timeDirection = -1;
    else if(_timeCurrent < 0.0f)
        _timeDirection = 1;
    
    _timeCurrent += _timeDirection * self.timeSinceLastUpdate;
    
    if (_updatingRotation) {
        // apply a low pass filter to rotation input
        const double alpha = 0.5;   // strength
        CMQuaternion cmquat = self.motionManager.deviceMotion.attitude.quaternion;
        static RZQuaternion<double> oldQuat(0,0,0,1);
        RZQuaternion<double> quat(cmquat.x, cmquat.y, cmquat.z, cmquat.w);
        Slerp(oldQuat, quat, oldQuat, alpha);
        /*
         const float alpha = 0.5f;   // filter strength
         static GLKQuaternion oldGQuat = GLKQuaternionMake(quat.x, quat.y, quat.z, quat.w);
         GLKQuaternion gquat = GLKQuaternionMake(quat.x, quat.y, quat.z, quat.w);
         oldGQuat = GLKQuaternionSlerp(oldGQuat, gquat, alpha);
         
         GLKVector3 a = GLKQuaternionRotateVector3(oldGQuat, GLKVector3Make(1, 0, 0));
         GLKVector3 b = GLKQuaternionRotateVector3(oldGQuat, GLKVector3Make(0, 1, 0));
         GLKVector3 c = GLKQuaternionRotateVector3(oldGQuat, GLKVector3Make(0, 0, 1));
         */
        
        if (tiler3)         tiler3->applyOrientation(oldQuat);
        else if (tiler4)    tiler4->applyOrientation(oldQuat);
        else if (tiler5)    tiler5->applyOrientation(oldQuat);
        else if (tiler6)    tiler6->applyOrientation(oldQuat);
        else if (tiler7)    tiler7->applyOrientation(oldQuat);
        else if (tiler8)    tiler8->applyOrientation(oldQuat);
        else if (tiler9)    tiler9->applyOrientation(oldQuat);
        else if (tiler10)   tiler10->applyOrientation(oldQuat);
        else if (tiler11)   tiler11->applyOrientation(oldQuat);
        else if (tiler12)   tiler12->applyOrientation(oldQuat);
    }
    
    const NSUInteger maxOperationCount = 16;
    if (updateQueue.operationCount < maxOperationCount) {
        [updateQueue addOperationWithBlock:^{
            if (tiler3)         tiler3->doPoints();
            else if (tiler4)    tiler4->doPoints();
            else if (tiler5)    tiler5->doPoints();
            else if (tiler6)    tiler6->doPoints();
            else if (tiler7)    tiler7->doPoints();
            else if (tiler8)    tiler8->doPoints();
            else if (tiler9)    tiler9->doPoints();
            else if (tiler10)   tiler10->doPoints();
            else if (tiler11)   tiler11->doPoints();
            else if (tiler12)   tiler12->doPoints();
            frameCount++;
        }];
    }
}

// touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    _updatingRotation = false;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = touches.anyObject;
    
    CGPoint point = [touch locationInView:self.view];
    CGPoint lastPoint = [touch previousLocationInView:self.view];
    
    if (tiler3)         tiler3->touchesMoved(point, lastPoint);
    else if (tiler4)    tiler4->touchesMoved(point, lastPoint);
    else if (tiler5)    tiler5->touchesMoved(point, lastPoint);
    else if (tiler6)    tiler6->touchesMoved(point, lastPoint);
    else if (tiler7)    tiler7->touchesMoved(point, lastPoint);
    else if (tiler8)    tiler8->touchesMoved(point, lastPoint);
    else if (tiler9)    tiler9->touchesMoved(point, lastPoint);
    else if (tiler10)   tiler10->touchesMoved(point, lastPoint);
    else if (tiler11)   tiler11->touchesMoved(point, lastPoint);
    else if (tiler12)   tiler12->touchesMoved(point, lastPoint);
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    _updatingRotation = true;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    _updatingRotation = true;
}

- (void)randomiseOrientation:(NSNotification*)note {
    if (tiler3)         tiler3->randomiseOrientation();
    else if (tiler4)    tiler4->randomiseOrientation();
    else if (tiler5)    tiler5->randomiseOrientation();
    else if (tiler6)    tiler6->randomiseOrientation();
    else if (tiler7)    tiler7->randomiseOrientation();
    else if (tiler8)    tiler8->randomiseOrientation();
    else if (tiler9)    tiler9->randomiseOrientation();
    else if (tiler10)   tiler10->randomiseOrientation();
    else if (tiler11)   tiler11->randomiseOrientation();
    else if (tiler12)   tiler12->randomiseOrientation();
    
    if (IDIOM != IPAD)
        [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)selectedColourScheme:(NSNotification*)note {
    self.currentImage = nil;
    if (_imageContext) {
        CFRelease(_imageContext);
        _imageContext = NULL;
    }
    int row = [note.object intValue];
    
    if (tiler3)         tiler3->applyColorScheme(ColourSchemes[row]);
    else if (tiler4)    tiler4->applyColorScheme(ColourSchemes[row]);
    else if (tiler5)    tiler5->applyColorScheme(ColourSchemes[row]);
    else if (tiler6)    tiler6->applyColorScheme(ColourSchemes[row]);
    else if (tiler7)    tiler7->applyColorScheme(ColourSchemes[row]);
    else if (tiler8)    tiler8->applyColorScheme(ColourSchemes[row]);
    else if (tiler9)    tiler9->applyColorScheme(ColourSchemes[row]);
    else if (tiler10)   tiler10->applyColorScheme(ColourSchemes[row]);
    else if (tiler11)   tiler11->applyColorScheme(ColourSchemes[row]);
    else if (tiler12)   tiler12->applyColorScheme(ColourSchemes[row]);
    
    if (IDIOM != IPAD)
        [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dimension:(NSNotification*)note {
    const int dim = [note.object intValue];
    
    dispatch_sync(dataQueue1, ^{
        dispatch_sync(dataQueue2, ^{
            [updateQueue cancelAllOperations];
            [updateQueue addOperationWithBlock:^{
                if (tiler3)         delete tiler3;
                else if (tiler4)    delete tiler4;
                else if (tiler5)    delete tiler5;
                else if (tiler6)    delete tiler6;
                else if (tiler7)    delete tiler7;
                else if (tiler8)    delete tiler8;
                else if (tiler9)    delete tiler9;
                else if (tiler10)   delete tiler10;
                else if (tiler11)   delete tiler11;
                else if (tiler12)   delete tiler12;
                
                tiler3 = nullptr;
                tiler4 = nullptr;
                tiler5 = nullptr;
                tiler6 = nullptr;
                tiler7 = nullptr;
                tiler8 = nullptr;
                tiler9 = nullptr;
                tiler10 = nullptr;
                tiler11 = nullptr;
                tiler12 = nullptr;
                
                switch (dim) {
                    case 3: tiler3 = new Tiler<3>(dataQueue1,dataQueue2,_aspectRatio);  break;
                    case 4: tiler4 = new Tiler<4>(dataQueue1,dataQueue2,_aspectRatio);  break;
                    case 5: tiler5 = new Tiler<5>(dataQueue1,dataQueue2,_aspectRatio);  break;
                    case 6: tiler6 = new Tiler<6>(dataQueue1,dataQueue2,_aspectRatio);  break;
                    case 7: tiler7 = new Tiler<7>(dataQueue1,dataQueue2,_aspectRatio);  break;
                    case 8: tiler8 = new Tiler<8>(dataQueue1,dataQueue2,_aspectRatio);  break;
                    case 9: tiler9 = new Tiler<9>(dataQueue1,dataQueue2,_aspectRatio);  break;
                    case 10: tiler10 = new Tiler<10>(dataQueue1,dataQueue2,_aspectRatio);  break;
                    case 11: tiler11 = new Tiler<11>(dataQueue1,dataQueue2,_aspectRatio);  break;
                    case 12: tiler12 = new Tiler<12>(dataQueue1,dataQueue2,_aspectRatio);  break;
                        
                    default:
                        [NSException raise:@"Tiles" format:@"Invalid dimension number %d", dim];
                        break;
                }
                
                [self randomiseOrientation:nil];
            }];
        });
    });
    
    if (IDIOM != IPAD)
        [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)selectedImage:(NSNotification*)note {
    if (self.currentPopoverController)
        [self.currentPopoverController dismissPopoverAnimated:NO];
    else
        [self dismissViewControllerAnimated:YES completion:nil];
    UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
    ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    ipc.delegate = self;
    
    if (IDIOM == IPAD) {
        UIPopoverController *popOver = [[UIPopoverController alloc] initWithContentViewController:ipc];
        self.currentPopoverController = popOver;
        [self.currentPopoverController presentPopoverFromRect:CGRectMake(450.0f, 825.0f, 10.0f, 10.0f) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        self.currentPopoverController.delegate = self;
    }
}

- (void)longPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        if (self.currentPopoverController)
            [self.currentPopoverController dismissPopoverAnimated:NO];
        else
            [self dismissViewControllerAnimated:YES completion:nil];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ColoursSchemeMenu *menu = [storyboard instantiateViewControllerWithIdentifier:@"coloursMenu"];
        
        if (IDIOM == IPAD) {
            self.currentPopoverController = [[UIPopoverController alloc] initWithContentViewController:menu];
            CGPoint point = [sender locationInView:self.view];
            [self.currentPopoverController presentPopoverFromRect:CGRectMake(point.x, point.y, 1, 1) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            self.currentPopoverController.delegate = self;
        }
        else {
            [self presentViewController:menu animated:YES completion:nil];
        }
    }
}

- (void)pinch:(UIPinchGestureRecognizer*)sender {
    if (tiler3)         tiler3->pinch(sender.velocity);
    else if (tiler4)    tiler4->pinch(sender.velocity);
    else if (tiler5)    tiler5->pinch(sender.velocity);
    else if (tiler6)    tiler6->pinch(sender.velocity);
    else if (tiler7)    tiler7->pinch(sender.velocity);
    else if (tiler8)    tiler8->pinch(sender.velocity);
    else if (tiler9)    tiler9->pinch(sender.velocity);
    else if (tiler10)   tiler10->pinch(sender.velocity);
    else if (tiler11)   tiler11->pinch(sender.velocity);
    else if (tiler12)   tiler12->pinch(sender.velocity);
}

#pragma mark - image picker delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    if (_imageContext) {
        CFRelease(_imageContext);
        _imageContext = NULL;
    }
    self.currentImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    _imageContext = CreateARGBBitmapContext(self.currentImage.CGImage);
    if (_imageContext == NULL) {
        // error creating context
        return;
    }
    
    // Get image width, height. We'll use the entire image.
    size_t w = CGImageGetWidth(self.currentImage.CGImage);
    size_t h = CGImageGetHeight(self.currentImage.CGImage);
    CGRect rect = {{0.f,0.f},{(CGFloat)w,(CGFloat)h}};
    
    // Draw the image to the bitmap context. Once we draw, the memory
    // allocated for the context for rendering will then contain the
    // raw image data in the specified color space.
    CGContextDrawImage(_imageContext, rect, self.currentImage.CGImage);
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - popover delegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    if (popoverController == self.currentPopoverController)
        self.currentPopoverController = nil;
}

CGContextRef CreateARGBBitmapContext (CGImageRef inImage)
{
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    void *          bitmapData;
    size_t             bitmapByteCount;
    size_t             bitmapBytesPerRow;
    
    // Get image width, height. We'll use the entire image.
    size_t pixelsWide = CGImageGetWidth(inImage);
    size_t pixelsHigh = CGImageGetHeight(inImage);
    
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (pixelsWide * 4);
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    // Use the generic RGB color space.
    colorSpace = CGColorSpaceCreateDeviceRGB();
    if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
        return NULL;
    }
    
    // Allocate memory for image data. This is the destination in memory
    // where any drawing to the bitmap context will be rendered.
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Memory not allocated!");
        CGColorSpaceRelease( colorSpace );
        return NULL;
    }
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
    // per component. Regardless of what the source image format is
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    context = CGBitmapContextCreate (bitmapData,
                                     pixelsWide,
                                     pixelsHigh,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        free (bitmapData);
        fprintf (stderr, "Context not created!");
    }
    
    // Make sure and release colorspace before returning
    CGColorSpaceRelease( colorSpace );
    
    return context;
}

@end
