//
//  RZColourPalette.cpp
//  Tiles
//
//  Created by Ruben Zilibowitz on 11/01/2015.
//  Copyright (c) 2015 Zilibowitz Productions. All rights reserved.
//

#include "RZColourPalette.h"

const std::vector<unsigned long> RZColourPalette::AdriftInDreams = {0xCFF09E, 0xA8DBA8, 0x79BD9A, 0x3B8686, 0x0B486B};
const std::vector<unsigned long> RZColourPalette::GiantGoldfish = {0x69D2E7, 0xA7DBD8, 0xE0E4CC, 0xF38630, 0xFA6900};
const std::vector<unsigned long> RZColourPalette::GoodFriends = {0xD9CEB2, 0x948C75, 0xD5DED9, 0x7A6A53, 0x99B2B7};
const std::vector<unsigned long> RZColourPalette::MellonBallSurprise = {0xD1F2A5, 0xEFFAB4, 0xFFC48C, 0xFF9F80, 0xF56991};
const std::vector<unsigned long> RZColourPalette::VintageModern = {0x8C2318, 0x5E8C6A, 0x88A65E, 0xBFB35A, 0xF2C45A};

extern "C"  const std::vector< const std::vector<unsigned long>* > ColourSchemes = {
    &RZColourPalette::AdriftInDreams,
    &RZColourPalette::GiantGoldfish,
    &RZColourPalette::GoodFriends,
    &RZColourPalette::MellonBallSurprise,
    &RZColourPalette::VintageModern
};

extern "C" const size_t NumColourSchemes = ColourSchemes.size();
