//
//  HelpViewController.h
//  Tiles
//
//  Created by Ruben Zilibowitz on 30/05/2015.
//  Copyright (c) 2015 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController
@property IBOutlet UIWebView *webView;
- (IBAction)closeHelp:(id)sender;
@end
