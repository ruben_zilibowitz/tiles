//
//  AppDelegate.h
//  Tiles
//
//  Created by Ruben Zilibowitz on 2/01/2015.
//  Copyright (c) 2015 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

