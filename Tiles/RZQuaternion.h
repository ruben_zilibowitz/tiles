//
//  RZQuaternion.h
//  Tiles
//
//  Created by Ruben Zilibowitz on 12/01/2015.
//  Copyright (c) 2015 Zilibowitz Productions. All rights reserved.
//

#ifndef __Tiles__RZQuaternion__
#define __Tiles__RZQuaternion__

// ------------------------------------------------------------------------
// This program is complementary material for the book:
//
// Frank Nielsen
//
// Visual Computing: Geometry, Graphics, and Vision
//
// ISBN: 1-58450-427-7
//
// Charles River Media, Inc.
//
//
// All programs are available at http://www.charlesriver.com/visualcomputing/
//
// You may use this program for ACADEMIC and PERSONAL purposes ONLY.
//
//
// The use of this program in a commercial product requires EXPLICITLY
// written permission from the author. The author is NOT responsible or
// liable for damage or loss that may be caused by the use of this program.
//
// Copyright (c) 2005. Frank Nielsen. All rights reserved.
// ------------------------------------------------------------------------

// ------------------------------------------------------------------------
// Modified by Ruben Zilibowitz (12th Jan 2015)
// To use templates and added some stuff and removed other stuff.
// ------------------------------------------------------------------------


#include <cmath>

template <class Tp>
class RZQuaternion {
public:
    class Point3D {
    public:
        Tp x,y,z;
        Point3D() : x(0), y(0), z(0) {}
        Point3D(Tp ax, Tp ay, Tp az) : x(ax), y(ay), z(az) {}
    } u;
    
    Tp w;
    
    RZQuaternion() : u(0,0,0), w(0) {}
    RZQuaternion(Tp ax, Tp ay, Tp az, Tp aw) : u(ax,ay,az), w(aw) {}
    
    inline void Multiply(const RZQuaternion q) {
        RZQuaternion tmp;
        tmp.u.x = ((w * q.u.x) + (u.x * q.w) + (u.y * q.u.z) - (u.z * q.u.y));
        tmp.u.y = ((w * q.u.y) - (u.x * q.u.z) + (u.y * q.w) + (u.z * q.u.x));
        tmp.u.z = ((w * q.u.z) + (u.x * q.u.y) - (u.y * q.u.x) + (u.z * q.w));
        tmp.w = ((w * q.w) - (u.x * q.u.x) - (u.y * q.u.y) - (u.z * q.u.z));
        *this = tmp;
    }
    
    inline Tp Norm() const {
        return sqrt(u.x*u.x+u.y*u.y+u.z*u.z+w*w);
    }
    
    
    inline void Normalize() {
        Tp norm=Norm();
        u.x/=norm;u.y/=norm;u.z/=norm;
    }
    
    inline void Conjugate() {
        u.x=-u.x;
        u.y=-u.y;
        u.z=-u.z;
    }
    
    inline void Inverse() {
        Tp norm=Norm();
        Conjugate();
        u.x/=norm;
        u.y/=norm;
        u.z/=norm;
        w/=norm;
    }
    
    RZQuaternion RotatePoint(const RZQuaternion::Point3D &pt) const {
        RZQuaternion q, p, qinv;
        
        p.w=0;
        p.u=pt;
        
        q = *this;
        qinv=q;
        qinv.Inverse();
        
        q.Multiply(p);
        q.Multiply(qinv);
        
        return q;
    }
};

template <class Tp>
inline void Slerp(RZQuaternion<Tp> q1, RZQuaternion<Tp> q2, RZQuaternion<Tp> &qr, Tp lambda) {
    Tp dotproduct = q1.u.x * q2.u.x + q1.u.y * q2.u.y + q1.u.z * q2.u.z + q1.w * q2.w;
    Tp theta, st, sut, sout, coeff1, coeff2;
    
    // algorithm adapted from Shoemake's paper
    lambda=lambda/2.0;
    
    theta = (Tp) std::acos(dotproduct);
    if (theta<0.0) theta=-theta;
    
    st = (Tp) std::sin(theta);
    sut = (Tp) std::sin(lambda*theta);
    sout = (Tp) std::sin((1-lambda)*theta);
    coeff1 = sout/st;
    coeff2 = sut/st;
    
    qr.u.x = coeff1*q1.u.x + coeff2*q2.u.x;
    qr.u.y = coeff1*q1.u.y + coeff2*q2.u.y;
    qr.u.z = coeff1*q1.u.z + coeff2*q2.u.z;
    qr.w = coeff1*q1.w + coeff2*q2.w;
    
    qr.Normalize();
}

#endif /* defined(__Tiles__RZQuaternion__) */
