//
//  Lattice_Zn.h
//  Tiles
//
//  Created by Ruben Zilibowitz on 3/01/2015.
//  Copyright (c) 2015 Zilibowitz Productions. All rights reserved.
//

#ifndef __Tiles__Lattice_Zn__
#define __Tiles__Lattice_Zn__

#include "EmitterTemplate.h"
#include <utility>
#include <array>
#include <list>
#include <vector>
#include <set>
#include <tuple>
#include <cmath>
#include <cassert>
#include <iostream>
#include "RZMatrix.h"

#include <dispatch/dispatch.h>

//#include "RGBfromHSV.h"
#include "RZColourPalette.h"

static dispatch_queue_t lock_queue1 = dispatch_queue_create("lock1", NULL);
static dispatch_queue_t lock_queue2 = dispatch_queue_create("lock2", NULL);

template <int dim>
class Lattice_Zn {
public:
    typedef std::array<float, dim> NVector;
    typedef std::array<int, dim> NLatticePoint;
    typedef std::tuple<NLatticePoint, NLatticePoint, NLatticePoint, NLatticePoint> NFace;
    struct ImageData {
        const UInt8 *pixels;
        size_t width, height;
        UInt8 pixelComponents;
    };
    
private:
    const float _aspectRatio;
    const float center_u, center_v;
    const size_t _minIterationsForConcurrency;
    const float _tileShrinkage;
    const float _scale;
    Emitter *_emitter;
    long remaining;
    NVector _generatorX, _generatorY;
    NVector _offset;
    float _radiusSquared;
    std::set<NFace> drawnFaces;
    const unsigned long TwoToTheN;
    RZMatrix<dim, dim, RZColourPalette::RGB> _colourMatrix;
    const RZColourPalette *_colourPalette;
    const ImageData *_imageData;
    
    constexpr float sqr(float x) const {
        return x*x;
    }
    
    constexpr unsigned long ipow(unsigned long base, unsigned char exponent) const {
        return exponent == 0 ? 1 : (base * ipow(base,exponent-1));
    }
    
    constexpr int iabs(int x) const {
        return (x < 0 ? -x : x);
    }
    
    NVector LatticePointToVector(const NLatticePoint &point) {
        NVector result;
        for (int i = 0; i < dim; i++) {
            result[i] = point[i];
        }
        return result;
    }
    
    float NVectorDotProduct(const NVector &A, const NVector &B) {
        float result = 0.f;
        for (int i = 0; i < dim; i++) {
            result += (A[i] * B[i]);
        }
        return result;
    }
    
    NVector planePointFromParameters(float u, float v) {
        NVector result;
        for (int i = 0; i < dim; i++) {
            result[i] = _generatorX[i]*u + _generatorY[i]*v + _offset[i];
        }
        return result;
    }
    
    NVector projectPointToPlane(const NVector &Q) {
        const float u = NVectorDotProduct(_generatorX, Q);
        const float v = NVectorDotProduct(_generatorY, Q);
        return planePointFromParameters(u, v);
    }
    
    float distanceSquaredBetween(const NVector &A, const NVector &B) {
        float result = 0.f;
        for (int i = 0; i < dim; i++) {
            result += sqr(A[i] - B[i]);
        }
        return result;
    }
    
    float distanceSquaredToPlane(const NVector &Q) {
        const NVector P = projectPointToPlane(Q);
        return distanceSquaredBetween(P, Q);
    }
    
    float distanceSquaredToPlaneFromLattice(const NLatticePoint &p) {
        return distanceSquaredToPlane(LatticePointToVector(p));
    }
    
    bool latticePointWithinBounds(const NLatticePoint &Q) {
        // this checks to see if the point is within the screen bounds
        const float u = (NVectorDotProduct(_generatorX, LatticePointToVector(Q)) + center_u) * _scale;
        const float v = (NVectorDotProduct(_generatorY, LatticePointToVector(Q)) + center_v) * _scale;
        const float maxV = MAX(MIN(_scale / 0.2f, 1.4f), 0.99f);
        const float maxU = _aspectRatio * maxV;
        return (-maxU < u && u < maxU && -maxV < v && v < maxV);
    }
    
    std::pair<float,float> line_solve(const std::tuple<float,float,float> &l1, const std::tuple<float,float,float> &l2) {
        const float a = std::get<0>(l1);
        const float b = std::get<1>(l1);
        const float c = std::get<2>(l1);
        
        const float d = std::get<0>(l2);
        const float e = std::get<1>(l2);
        const float f = std::get<2>(l2);
        
        return std::make_pair((e*c-b*f)/(b*d-a*e), (a*f-d*c)/(b*d-a*e));
    }
    
    bool latticePointIncluded(const NLatticePoint &Q) {
        if (distanceSquaredToPlaneFromLattice(Q) > _radiusSquared) {
            return false;
        }
        
        if (!latticePointWithinBounds(Q)) {
            return false;
        }
        
        // Need to check feasibility of a system of linear inequalities.
        // Q <= P(u,v) + 1/2 < Q + 1,
        
        // crossings matrix
        // NB: is it ok to use nested types like this??
        std::array<std::array<std::array<std::pair<float, float>, 4>, dim>, dim> intersections;
        bool foundIntersections = false;
        
        // Step 1
        // generate all intersection points of these lines
        //_generatorX[i]*u + _generatorY[i]*v + _offset[i] - Q[i] - 0.5f;
        //_generatorX[i]*u + _generatorY[i]*v + _offset[i] - Q[i] + 0.5f;
        for (int i = 0; i < dim; i++) {
            for (int j = i+1; j < dim; j++) {
                int k = 0;
                for (float g = -0.5f; g < 1; g += 1.f) {
                    for (float h = -0.5f; h < 1; h += 1.f) {
                        auto crossing = line_solve(std::make_tuple(_generatorX[i],_generatorY[i],_offset[i] - Q[i] + g),
                                                   std::make_tuple(_generatorX[j],_generatorY[j],_offset[j] - Q[j] + h));
                        if (::isfinite(crossing.first) && ::isfinite(crossing.second)) {
                            intersections[i][j][k] = crossing;
                            intersections[j][i][k] = crossing;
                            foundIntersections = true;
                        }
                        else {
                            // the crossing point is at infinity
                            // just use a big number to represent this
                            intersections[i][j][k] = std::make_pair(10000, 10000);
                            intersections[j][i][k] = std::make_pair(10000, 10000);
                        }
                        k++;
                    }
                }
            }
        }
        
        // if no lines intersect then we are good
        if (! foundIntersections) {
            return true;
        }
        
        // Step 2
        // Prune points that do not agree with these linear inequalities
        //_generatorX[i]*u + _generatorY[i]*v + _offset[i] < Q[i] + 0.5f;
        //_generatorX[i]*u + _generatorY[i]*v + _offset[i] > Q[i] - 0.5f;
        for (int i = 0; i < dim; i++) {
            for (int j = i+1; j < dim; j++) {
                for (int k = 0; k < 4; k++) {
                    const auto P = intersections[i][j][k];
                    bool sat = true;
                    for (int D = 0; D < dim; D++) {
                        if (D != i && D != j) {
                            const float x0 = _generatorX[D]*P.first + _generatorY[D]*P.second + _offset[D] - Q[D];
                            const float y0 = x0 - 0.5f, y1 = x0 + 0.5f;
                            if (y0 > 0 || y1 < 0) {
                                sat = false;
                                break;
                            }
                        }
                    }
                    if (sat) {
                        // At least one point is satisfied.
                        // The inequalities are ok.
                        return true;
                    }
                }
            }
        }
        
        // If we get here then none of the intersection points were satisfied
        return false;
    }
    
#if 0
    bool latticePointIncluded(const NLatticePoint &Q) {
        // This can be used only if dim <= 3
        // return (distanceSquaredToPlaneFromLattice(p) < _radiusSquared);
        // Otherwise a different method is needed...
        
        if (distanceSquaredToPlaneFromLattice(Q) > _radiusSquared) {
            return false;
        }
        
        // Need to check feasibility of a system of linear inequalities.
        // Q <= P(u,v) + 1/2 < Q + 1,
        // where P(u,v) is the plane equation in parameters u and v.
        // Could use a linear programming algorithm.
        // Also can use Fourier-Motzkin elimination (http://en.wikipedia.org/wiki/Fourier–Motzkin_elimination).
        // Here is an implementation of this method
        // Number of steps is roughly 4*(dim/2)^4
        
        std::vector< std::tuple<float,float> > system_u_ge, system_u_le;
        std::vector< float > system_v_le, system_v_ge;
        
        // Step 1
        // Eliminate u
        // Separate inequalities into u < ... and ... < u classes
        // This creates at most 2*dim inequalities
        for (int i = 0; i < dim; i++) {
            auto u_le = std::make_tuple((Q[i] - _offset[i] + 0.5f)/_generatorX[i], -_generatorY[i]/_generatorX[i]);
            auto u_ge = std::make_tuple((Q[i] - _offset[i] - 0.5f)/_generatorX[i], -_generatorY[i]/_generatorX[i]);
            if (_generatorX[i] > 0) {
                system_u_ge.push_back(u_ge);
                system_u_le.push_back(u_le);
            }
            else if (_generatorX[i] < 0) {
                system_u_le.push_back(u_ge);
                system_u_ge.push_back(u_le);
            }
            else if (_generatorX[i] == 0) {
                // In this case we only have an inequality involving v
                float v_le = (Q[i] + 0.5f - _offset[i]) / _generatorY[i];
                float v_ge = (Q[i] - 0.5f - _offset[i]) / _generatorY[i];
                if (_generatorY[i] > 0) {
                    system_v_le.push_back(v_le);
                    system_v_ge.push_back(v_ge);
                }
                else if (_generatorY[i] < 0) {
                    system_v_le.push_back(v_ge);
                    system_v_ge.push_back(v_le);
                }
                else if (_generatorY[i] == 0) {
                    // In this case it is an inequality involving constants only
                    // Q <= P(u,v) + 1/2 < Q + 1
                    if (Q[i] - 0.5f > _offset[i]) {
                        return false;
                    }
                    if (Q[i] + 0.5f < _offset[i]) {
                        return false;
                    }
                }
            }
        }
        
        // Step 2
        // Eliminate v
        // For each pair of inequalities from the two classes,
        // solve for v to create a new inequality.
        for (auto u_le : system_u_le) {
            for (auto u_ge : system_u_ge) {
                float a = std::get<0>(u_ge);
                float b = std::get<1>(u_ge);
                float c = std::get<0>(u_le);
                float d = std::get<1>(u_le);
                float bmd = b - d;
                if (bmd > 0) {
                    system_v_le.push_back((c-a)/bmd);
                }
                else if (bmd < 0) {
                    system_v_ge.push_back((c-a)/bmd);
                    
                }
                else if (bmd == 0) {
                    // 0 <= c - a must hold for the system to be feasible
                    if (0 > c - a) {
                        return false;
                    }
                }
            }
        }
        
        // Step 3
        // Now we just have a system involving constants to check
        for (auto v_le : system_v_le) {
            for (auto v_ge : system_v_ge) {
                // v_ge <= v_le must hold for the system to be feasible
                if (v_ge > v_le) {
                    return false;
                }
            }
        }
        
        return true;
    }
#endif
    
    void emitVertex(const float &cu, const float &cv, const NLatticePoint &P, const RZColourPalette::RGB &colour) {
        float u = NVectorDotProduct(_generatorX, LatticePointToVector(P));
        float v = NVectorDotProduct(_generatorY, LatticePointToVector(P));
        
        float du = u-cu, dv = v-cv;
        float dist = sqrtf(du*du + dv*dv);
        
        // shrink tile to allow for border
        u = cu + du - du/dist*_tileShrinkage;
        v = cv + dv - dv/dist*_tileShrinkage;
        
        _emitter->particles[remaining].location[0] = (u + center_u) * _scale;
        _emitter->particles[remaining].location[1] = (v + center_v) * _scale;
        _emitter->particles[remaining].shade[0] = colour.red;
        _emitter->particles[remaining].shade[1] = colour.green;
        _emitter->particles[remaining].shade[2] = colour.blue;
        
        (remaining) ++;
    }
    
    NVector faceCenter(const NFace &face) {
        NVector center;
        for (int i = 0; i < dim; i++) {
            center[i] = (std::get<0>(face)[i] + std::get<1>(face)[i] + std::get<2>(face)[i] + std::get<3>(face)[i]) * 0.25f;
        }
        return center;
    }
    
    void drawTile(const NFace &face, const RZColourPalette::RGB &acolour) {
        if (remaining < NUM_PARTICLES) {
            
            // project face onto plane and store
            // face so we don't draw it again
            
            const NVector C = faceCenter(face);
            const float cu = NVectorDotProduct(_generatorX, C);
            const float cv = NVectorDotProduct(_generatorY, C);
            
            RZColourPalette::RGB colour = acolour;
            if (_imageData) {
//                long x = (cu - 2*center_u) * 0.02f * _imageData->width/2 + _imageData->width/2;
//                long y = (cv - 2*center_v) * 0.02f * _imageData->width/2 + _imageData->height/2;
                
                
                long x = (cu + center_u) * _scale * _imageData->width/2 + _imageData->width/2;
                long y = (cv + center_v) * _scale * _imageData->width/2 + _imageData->height/2;
                if (x < 0)
                    x = -x;
                else if (x >= _imageData->width)
                    x = 2*_imageData->width - x;
                if (y < 0)
                    y = -y;
                else if (y >= _imageData->height)
                    y = 2*_imageData->height - y;
                x = ((x % _imageData->width) + _imageData->width) % _imageData->width;
                y = ((y % _imageData->height) + _imageData->height) % _imageData->height;
                if (x >= 0 && y >= 0 && x < _imageData->width && y < _imageData->height) {
                    long base = (x + _imageData->width*(_imageData->height-1 - y)) * _imageData->pixelComponents;
                    colour.red = ((UInt8*)_imageData->pixels)[base+1] / 255.f;
                    colour.green = ((UInt8*)_imageData->pixels)[base+2] / 255.f;
                    colour.blue = ((UInt8*)_imageData->pixels)[base+3] / 255.f;
                }
            }
            
            dispatch_sync(lock_queue2, ^{
                // triangle 1
                emitVertex(cu, cv, std::get<0>(face), colour);
                emitVertex(cu, cv, std::get<1>(face), colour);
                emitVertex(cu, cv, std::get<2>(face), colour);
                // triangle 2
                emitVertex(cu, cv, std::get<0>(face), colour);
                emitVertex(cu, cv, std::get<2>(face), colour);
                emitVertex(cu, cv, std::get<3>(face), colour);
            });
        }
    }
    
    bool regionIsUnitSquare(const NLatticePoint &r0, const NLatticePoint &r1) {
        int count = 0;
        for (int i = 0; i < dim; i++) {
            if (iabs(r0[i] - r1[i]) == 1) {
                count++;
            }
            else if (r0[i] != r1[i]) {
                // For a unit square each pair of coordinates should have
                // a difference of either zero or one.
                return false;
            }
            if (count >= 3) {
                // This region is not a square. It has higher dimensionality.
                return false;
            }
        }
        
        // There should be a separation of one along exactly
        // two axes.
        return (count == 2);
    }
    
    // gets the axes along which the square is parallel to
    std::pair<int,int> axesFromFace(const NFace &face) {
        std::vector<int> axes;
        axes.reserve(4);
        for (int i = 0; i < dim; i++) {
            if (std::get<0>(face)[i] != std::get<1>(face)[i])
                axes.push_back(i);
            else if (std::get<0>(face)[i] != std::get<2>(face)[i])
                axes.push_back(i);
            
            if (axes.size() >= 2)
                break;
        }
        assert(axes.size() == 2);
        return std::make_pair(std::min(axes[0], axes[1]),std::max(axes[0], axes[1]));
    }
    
    RZColourPalette::RGB colourFromFace(const NFace &face) {
        auto axes = axesFromFace(face);
        return _colourMatrix(axes.first,axes.second);
    }
    
    // returns all neighbouring lattice points that are close to the plane
    std::vector<NLatticePoint> neighbouringLatticePoints(const NLatticePoint &P) {
        std::vector<NLatticePoint> result;
        result.reserve(2*dim);
        for (int i = 0; i < dim; i++) {
            NLatticePoint Q;
            Q = P;
            Q[i] ++;
            if (latticePointIncluded(Q)) {
                result.push_back(Q);
            }
            Q = P;
            Q[i] --;
            if (latticePointIncluded(Q)) {
                result.push_back(Q);
            }
        }
        return result;
    }
    
    void printLatticePoint(const NLatticePoint &point) {
        for (int i = 0; i < dim; i++) {
            std::cout << point[i] << ",";
        }
        std::cout << std::endl;
    }
    
#pragma mark - public methods
public:
    Lattice_Zn(Emitter *inEmitter, const NVector &inGeneratorX, const NVector &inGeneratorY, const NVector &inOffset, const RZColourPalette *colourPalette, float inAspectRatio = 1024.f / 768.f, float centerU = 0, float centerV = 0, float scale = 0.16f, float tileBorderFactor = 0.02f, const ImageData *imageData = NULL) : _aspectRatio(inAspectRatio), _emitter(inEmitter), remaining(0), _generatorX(inGeneratorX), _generatorY(inGeneratorY), _offset(inOffset), TwoToTheN(ipow(2, dim)), _tileShrinkage(tileBorderFactor), _scale(scale), _minIterationsForConcurrency(4), center_u(centerU), center_v(centerV), _colourPalette(colourPalette), _imageData(imageData) {
        
        // dimension of embedding space must be at least 3
        assert(dim >= 3);
        
        // compute radius
        // iterate through the vertices of a unit cube centered on the plane
        // and find the distance of the furthest one from the plane.
        _radiusSquared = 0.f;
        for (unsigned long i = 0; i < TwoToTheN; i++) {
            NVector point;
            for (int k = 0; k < dim; k++) {
                const unsigned long digit = i >> k;
                point[k] = ((digit & 1) == 1 ? 0.5 : -0.5) + _offset[k];
            }
            _radiusSquared = ::fmaxf(_radiusSquared, distanceSquaredToPlane(point));
        }
        
        int k = 0;
        const float x = 1.f / (dim*(dim-1)/2);
        for (int i = 0; i < dim; i++) {
            for (int j = i+1; j < dim; j++) {
                RZColourPalette::RGB rgb = _colourPalette->colourFromPosition(k*x);
                _colourMatrix(i,j) = rgb;
                _colourMatrix(j,i) = rgb;
                k++;
            }
        }
    }
    
    void traverseLatticeFrom(float startU, float startV) {
        const unsigned long kMaxGen = 5000;
        unsigned long gen = 0;
        NVector P = planePointFromParameters(startU, startV);
        NLatticePoint LP;
        for (int i = 0; i < dim; i++) {
            LP[i] = (int)floorf(P[i] + 0.5f);
        }
        
        // LP "should" be in the staircase but due to rounding errors it may not be.
        // Then we can check the neighbours to find a point that is.
        if (!latticePointIncluded(LP)) {
            bool found = false;
            auto neighs = neighbouringLatticePoints(LP);
            for (auto ne : neighs) {
                if (latticePointIncluded(ne)) {
                    LP = ne;
                    found = true;
                    break;
                }
            }
            if (!found) {
                std::cerr << "Warning: could not find a starting point on the staircase" << std::endl;
            }
        }
        
        __block std::set<NLatticePoint> boundary, allPoints;
        __block std::vector<NLatticePoint> expandedBoundary;
        
        // starting condition
        boundary.insert(LP);
        
        // iterate
        while (remaining < NUM_PARTICLES && boundary.size() > 0 && gen < kMaxGen) {
            // clear this set to begin with
            expandedBoundary.clear();
            
            // for each point in the current boundary
            
            // NB: could optimise this further by avoiding threads for small numbers of iterations
            __block std::vector<NLatticePoint> boundaryVec;
            std::copy(boundary.begin(), boundary.end(), std::back_inserter(boundaryVec));
            auto myblock = ^(size_t idx) {
                if (remaining >= NUM_PARTICLES) {
                    return;
                }
                
                auto Q = boundaryVec[idx];
                {
                    // find all neighbouring points
                    auto ns = neighbouringLatticePoints(Q);
                    
                    // remove any that have already been processed
                    std::vector<NLatticePoint> neighbours;
                    for (auto n : ns) {
                        if (allPoints.find(n) == allPoints.end()) {
                            neighbours.push_back(n);
                        }
                    }
                    
                    // Now find all pairs of neighbours to Q that form the adjacent
                    // vertices of a face. Check if the opposite vertex is to be included.
                    // If so then we have a tile.
                    for (auto it = neighbours.begin(); it != neighbours.end(); it++) {
                        auto jt = it; ++jt;
                        for (; jt != neighbours.end(); jt++) {
                            const NLatticePoint Qn1 = *it;
                            const NLatticePoint Qn2 = *jt;
                            
                            // check if Q,Qn1,Qn2 are three vertices of a face
                            // this is equivalent to them NOT being collinear
                            bool isPotentialFace = false;
                            for (int i = 0; i < dim; i++) {
                                if (Q[i] - Qn1[i] != Qn2[i] - Q[i]) {
                                    isPotentialFace = true;
                                    break;
                                }
                            }
                            
                            if (isPotentialFace) {
                                // Find the fourth point of the face
                                // This will be the vector sum of Q + (Qn1 - Q) + (Qn2 - Q)
                                // Or just Qn1 + Qn2 - Q
                                NLatticePoint R;
                                for (int i = 0; i < dim; i++) {
                                    R[i] = Qn1[i] + Qn2[i] - Q[i];
                                }
                                
                                // For this to form a face (and hence a tile)
                                // The point R must be "close" to the plane.
                                if (latticePointIncluded(R)) {
                                    assert(regionIsUnitSquare(Q, R));
                                    NFace face = std::make_tuple(Q, Qn1, R, Qn2);
                                    drawTile(face, colourFromFace(face));
                                }
                            }
                        }
                        
                        // add the resulting points to the new boundary layer
                        dispatch_sync(lock_queue1, ^{
                            for (auto E : neighbours) {
                                expandedBoundary.push_back(E);
                            }
                        });
                    }
                }
            };
            
            // check if iterations is large enough use concurrency
            if (boundary.size() >= _minIterationsForConcurrency) {
                dispatch_queue_t the_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_apply(boundary.size(), the_queue, myblock);
            }
            else {
                for (size_t idx = 0; idx < boundary.size(); idx++) {
                    myblock(idx);
                }
            }
            
            // add the old boundary to the set of processed points
            for (auto S : boundary) {
                allPoints.insert(S);
            }
            
            // The new boundary becomes the old
            boundary.clear();
            for (auto S : expandedBoundary) {
                boundary.insert(S);
            }
            
            gen++;
        }
    }
};

#endif /* defined(__Tiles__Lattice_Zn__) */
