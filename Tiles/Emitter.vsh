// Vertex Shader

static const char* EmitterVS = STRINGIFY
(
 
 // Attributes
 attribute vec3 aShade;
 attribute vec2 aLocation;
 
 // Uniforms
 uniform mat4 uProjectionMatrix;
 uniform float uK;
 uniform float uTime;
 
 // Output to Fragment Shader
 varying vec3 vShade;
 
 void main(void)
{
    float x = aLocation[0];
    float y = aLocation[1];
    
    gl_Position = uProjectionMatrix * vec4(x, y, 0.0, 1.0);
    gl_PointSize = 16.0;
    
    vShade = aShade;
}
 
 );