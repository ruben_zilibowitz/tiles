//
//  ColoursSchemeMenu.m
//  Tiles
//
//  Created by Ruben Zilibowitz on 18/01/2015.
//  Copyright (c) 2015 Zilibowitz Productions. All rights reserved.
//

#import "ColoursSchemeMenu.h"

NSString * const RZRandomiseOrientation = @"RZRandomiseOrientation";
NSString * const RZColourNotification = @"RZColourNotification";
NSString * const RZDimensionNotification = @"RZDimensionNotification";
NSString * const RZOpenHelpNotification = @"RZOpenHelpNotification";

extern const size_t NumColourSchemes;

enum {
    kSection_Orientation = 0,
    kSection_Colours,
    kSection_Dimension,
    kSection_Help,
    kNumSections
};

@interface ColoursSchemeMenu ()

@end

@implementation ColoursSchemeMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return kNumSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == kSection_Orientation) {
        return 1;
    }
    if (section == kSection_Colours) {
        return NumColourSchemes;
    }
    else if (section == kSection_Dimension) {
        return (12-3+1);
    }
    else if (section == kSection_Help) {
        return 1;
    }
    else {
        return -1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (indexPath.section == kSection_Orientation) {
        cell.textLabel.text = @"Randomise Orientation";
    }
    else if (indexPath.section == kSection_Colours) {
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = @"Ocean";
                break;
            case 1:
                cell.textLabel.text = @"Tangerine";
                break;
            case 2:
                cell.textLabel.text = @"Steel";
                break;
            case 3:
                cell.textLabel.text = @"Pastel";
                break;
            case 4:
                cell.textLabel.text = @"Autumn";
                break;
                
            default:
                cell.textLabel.text = [NSString stringWithFormat:@"Colour scheme %ld", (long)indexPath.row];
                break;
        }
    }
    else if (indexPath.section == kSection_Dimension) {
        cell.textLabel.text = @(indexPath.row + 3).stringValue;
    }
    else if (indexPath.section == kSection_Help) {
        cell.textLabel.text = @"Help";
    }
    
    return cell;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == kSection_Orientation) {
        return @"Orientation";
    }
    if (section == kSection_Colours) {
        return @"Colour Scheme";
    }
    else if (section == kSection_Dimension) {
        return @"Dimension of Embedding";
    }
    else if (section == kSection_Help) {
        return @"Help";
    }
    else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == kSection_Orientation) {
        [[NSNotificationCenter defaultCenter] postNotificationName:RZRandomiseOrientation object:self];
    }
    else if (indexPath.section == kSection_Colours) {
        [[NSNotificationCenter defaultCenter] postNotificationName:RZColourNotification object:@(indexPath.row)];
    }
    else if (indexPath.section == kSection_Dimension) {
        [[NSNotificationCenter defaultCenter] postNotificationName:RZDimensionNotification object:@(indexPath.row+3)];
    }
    else if (indexPath.section == kSection_Help) {
        [self dismissViewControllerAnimated:YES completion:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:RZOpenHelpNotification object:self];
        }];
    }
}

@end
