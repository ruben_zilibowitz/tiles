//
//  RZColourPalette.h
//  Tiles
//
//  Created by Ruben Zilibowitz on 7/01/2015.
//  Copyright (c) 2015 Zilibowitz Productions. All rights reserved.
//

#ifndef Tiles_RZColourPalette_h
#define Tiles_RZColourPalette_h

#include <vector>
#include <cassert>

class RZColourPalette {
public:
    struct RGB {
        float red, green, blue;
        RGB() {}
        RGB(float r,float g,float b) : red(r), green(g), blue(b) {}
    };
    
protected:
    
    std::vector< std::pair<float,RGB> > colours;
    
public:
    RZColourPalette() {}
    
    void insertColour(const RGB &rgb, float position) {
        assert(0 <= position && position <= 1);
        
        std::vector< std::pair<float,RGB> >::iterator it;
        for (it = colours.begin(); it != colours.end(); it++) {
            if (position > it->first) {
                break;
            }
        }
        colours.insert(it,std::make_pair(position,rgb));
    }
    
    void pushBackColour(const RGB &rgb) {
        const float x = (float)colours.size() / (colours.size() + 1);
        for (auto &c : colours) {
            c.first *= x;
        }
        colours.push_back(std::make_pair(x,rgb));
    }
    
    void wrap() {
        assert(! colours.empty());
        colours.push_back(std::make_pair(1,colours[0].second));
    }
    
    void paletteFromHexVector(const std::vector<unsigned long> &vec) {
        for (auto hex : vec) {
            unsigned char blue = hex & 0xff;
            unsigned char green = (hex >> 8) & 0xff;
            unsigned char red = (hex >> 16) & 0xff;
            pushBackColour(RGB(red/255.f,green/255.f,blue/255.f));
        }
        wrap();
    }
    
    RGB colourFromPosition(float position) const {
        assert(0 <= position && position <= 1);
        std::vector< std::pair<float,RGB> >::const_iterator it, jt;
        for (it = colours.cbegin(); it != colours.cend(); it++) {
            if (it->first > position) {
                break;
            }
        }
        assert(it != colours.begin());
        jt = it;
        it--;
        
        const RGB colourA = it->second, colourB = jt->second;
        const float a = position - it->first;
        const float b = jt->first - position;
        const float apb = a + b;
        const float alpha = b / apb, beta = a / apb;
        const RGB result(
         colourA.red*alpha + colourB.red*beta,
         colourA.green*alpha + colourB.green*beta,
         colourA.blue*alpha + colourB.blue*beta);
        return result;
    }
    
    RGB colourFromIndex(unsigned int idx) {
        return colours[idx].second;
    }
    
    const static std::vector<unsigned long> AdriftInDreams;
    const static std::vector<unsigned long> GiantGoldfish;
    const static std::vector<unsigned long> GoodFriends;
    const static std::vector<unsigned long> MellonBallSurprise;
    const static std::vector<unsigned long> VintageModern;
};

#endif
