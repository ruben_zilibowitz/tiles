//
//  GameViewController.h
//  Tiles
//
//  Created by Ruben Zilibowitz on 2/01/2015.
//  Copyright (c) 2015 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface GameViewController : GLKViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIPopoverControllerDelegate>

- (float)randomFloatBetween:(float)min :(float)max;

@end
