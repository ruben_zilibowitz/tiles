//
//  RZMatrix.h
//  Tiles
//
//  Created by Ruben Zilibowitz on 6/01/2015.
//  Copyright (c) 2015 Zilibowitz Productions. All rights reserved.
//

#ifndef __Tiles__RZMatrix__
#define __Tiles__RZMatrix__

#include <array>
#include <Accelerate/Accelerate.h>

// matrix class
template <int rows, int cols, class T>
class RZMatrix {
private:
    std::array<T, rows*cols> data;
    
public:
    void writeData(T* outputData) const {
        std::copy(data.begin(), data.end(), outputData);
    }
    
    void readData(const T* inputData) {
        std::copy(inputData, inputData + rows*cols, data.begin());
    }
    
    T &operator()(int r, int c) {
        return data[r*cols + c];
    }
    
    const T &operator()(int r, int c) const {
        return data[r*cols + c];
    }
    
    std::array<T, rows> operator*(const std::array<T, cols> &vec) {
        std::array<T, rows> result;
        for (int i = 0; i < rows; i++) {
            result[i] = 0.f;
            for (int j = 0; j < cols; j++) {
                result[i] += (*this)(i,j) * vec[j];
            }
        }
        return result;
    }
    
    RZMatrix &operator+=(const RZMatrix &rhs) {
        for (int i = 0; i < rows*cols; i++) {
            data[i] += rhs[i];
        }
        return *this;
    }
    
    RZMatrix() {}
};

// matrix addition
template <int rows, int cols, class T>
static inline RZMatrix<rows,cols,T> operator+(RZMatrix<rows,cols,T> lhs, const RZMatrix<rows,cols,T> &rhs) {
    lhs += rhs;
    return lhs;
}

// matrix multiplication
template <int rowsL, int colsL_rowsR, int colsR, class T>
inline RZMatrix<rowsL,colsR,T> operator*(const RZMatrix<rowsL,colsL_rowsR,T> &lhs, const RZMatrix<colsL_rowsR,colsR,T> &rhs) {
    RZMatrix<rowsL,colsR,T> result;
    for (int r = 0; r < rowsL; r++) {
        for (int c = 0; c < colsR; c++) {
            result(r,c) = 0;
            for (int k = 0; k < colsL_rowsR; k++) {
                result(r,c) += lhs(r,k)*rhs(k,c);
            }
        }
    }
    return result;
}

// http://en.wikipedia.org/wiki/Skew-symmetric_matrix
template <int N, class T>
static inline RZMatrix<N,N,T> SkewSymmetricMatrix(const std::array<T, N*(N-1)/2> &input) {
    RZMatrix<N,N,T> result;
    int k = 0;
    for (int r = 0; r < N; r++) {
        result(r,r) = 0;    // diagonal must be zeroes
        for (int c = 0; c < r; c++) {
            bool sign = (r+c)%2==1;
            result(r,c) = (sign ? input[k] : -input[k]);
            result(c,r) = (sign ? -input[k] : input[k]);
            k++;
        }
    }
    return result;
}

// square matrix with zeroes everywhere
template <int N, class T>
static inline RZMatrix<N,N,T> ZeroMatrix() {
    RZMatrix<N,N,T> result;
    for (int r = 0; r < N; r++) {
        for (int c = 0; c < N; c++) {
            result(r,c) = 0;
        }
    }
    return result;
}

// square matrix with zeroes everywhere except for diagonal which contains ones
template <int N, class T>
static inline RZMatrix<N,N,T> IdentityMatrix() {
    RZMatrix<N,N,T> result;
    for (int r = 0; r < N; r++) {
        for (int c = 0; c < N; c++) {
            result(r,c) = (r==c?1:0);
        }
    }
    return result;
}


template <int n>
static inline double det(const RZMatrix<n, n, double> &mat) {
    __CLPK_integer N = n;
    __CLPK_integer *IPIV = new __CLPK_integer[N+1];
    __CLPK_doublereal* A = new __CLPK_doublereal[N*N];
    __CLPK_integer INFO;
    
    mat.writeData(A);
    
    __CLPK_doublereal result = 1;
    
    dgetrf_(&N,&N,A,&N,IPIV,&INFO);
    
    for (int i = 0; i < N; i++) {
        result *= mat(i,i);
    }
    
    delete[] IPIV;
    delete[] A;
    
    return result;
}

// If we are using the double type then
// just use LAPACK for this.
template <int n>
static inline RZMatrix<n,n,double> InverseMatrix(const RZMatrix<n,n,double> &mat) {
    __CLPK_integer N = n;
    __CLPK_integer *IPIV = new __CLPK_integer[N+1];
    __CLPK_integer LWORK = N*N;
    __CLPK_doublereal *WORK = new __CLPK_doublereal[LWORK];
    __CLPK_doublereal* A = new __CLPK_doublereal[LWORK];
    __CLPK_integer INFO;
    
    mat.writeData(A);
    
    dgetrf_(&N,&N,A,&N,IPIV,&INFO);
    dgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);
    
    RZMatrix<n,n,double> result;
    result.readData(A);
    
    delete[] IPIV;
    delete[] WORK;
    delete[] A;
    
    return result;
}

// A basic Gaussian elimination solver is used here.
// Limitation: no zeroes allowed on the diagonal.
// Also precision may not be so great due to rounding errors in some cases.
template <int N, class T>
static inline RZMatrix<N,N,T> InverseMatrix(RZMatrix<N,N,T> A) {
    RZMatrix<N,N,T> result = IdentityMatrix<N,T>();
    
    // Use Gaussian elimination
    for (int pivotCol = 0; pivotCol < N; pivotCol++) {
        // divide row by pivot term
        // pivot term must be non-zero
        const auto piv = A(pivotCol,pivotCol);
        assert(piv != 0);
        for (int c = 0; c < N; c++) {
            A(pivotCol,c) /= piv;
            result(pivotCol,c) /= piv;
        }
        A(pivotCol,pivotCol) = 1;
        
        // minus this row from lower rows to cancel out column below this row
        for (int r = pivotCol+1; r < N; r++) {
            const auto x = A(r,pivotCol);
            if (x != 0) {
                for (int c = 0; c < N; c++) {
                    A(r,c) -= x * A(pivotCol,c);
                    result(r,c) -= x * result(pivotCol,c);
                }
                A(r,pivotCol) = 0;
            }
        }
    }
    
    // matrix A now is upper triangular with 1's on diagonal
    // Perform back substitution
    for (int pivotCol = 1; pivotCol < N; pivotCol++) {
        for (int r = 0; r < pivotCol; r++) {
            const auto x = A(r,pivotCol);
            if (x != 0) {
                for (int c = 0; c < N; c++) {
                    A(r,c) -= x * A(pivotCol,c);
                    result(r,c) -= x * result(pivotCol,c);
                }
            }
        }
    }
    
    // matrix A should now be the identity
    // matrix result should now contain the inverse of the initial A
    
    return result;
}

// http://en.wikipedia.org/wiki/Cayley_transform
template <int N, class T>
static inline RZMatrix<N,N,T> CayleyTransform(const std::array<T, N*(N-1)/2> &input) {
    RZMatrix<N,N,T> A = SkewSymmetricMatrix<N,T>(input);
    
    // L = I - A
    RZMatrix<N,N,T> L;
    for (int r = 0; r < N; r++) {
        for (int c = 0; c < N; c++) {
            L(r,c) = -A(r,c);
            if (r == c) {
                L(r,c) += 1;
            }
        }
    }
    
    // R = (I + A)^(-1)
    RZMatrix<N,N,T> R;
    for (int r = 0; r < N; r++) {
        for (int c = 0; c < N; c++) {
            R(r,c) = A(r,c);
            if (r == c) {
                R(r,c) += 1;
            }
        }
    }
    R = InverseMatrix(R);
    
    return L*R;
}

// stream output for matrices
template <int rows, int cols, class T>
std::ostream& operator<<(std::ostream& os, const RZMatrix<rows, cols, T>& mat) {
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < cols; c++) {
            os << mat(r,c) << " ";
        }
        os << std::endl;
    }
    return os;
}

#endif /* defined(__Tiles__RZMatrix__) */
